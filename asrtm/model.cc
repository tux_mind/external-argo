/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "argo/asrtm/model.h"


namespace argo { namespace asrtm {
  
Model::Model(OperatingPointID_t numberOP) {
  opFastAccess.resize(numberOP);
  opSet.resize(numberOP, SetType::NotPresent);
  
}

Model::Model(const Model& model) {

    for(std::list<OperatingPointID_t>::const_iterator id = model.S1.cbegin(); id != model.S1.cend(); ++id) {
        S1.push_back(*id);
    }

    for(std::list<OperatingPointID_t>::const_iterator id = model.S2.cbegin(); id != model.S2.cend(); ++id) {
        S2.push_back(*id);
    }

    opFastAccess.reserve(model.opFastAccess.size());
    for(std::vector<std::list<OperatingPointID_t>::iterator>::const_iterator it = model.opFastAccess.cbegin(); it != model.opFastAccess.cend(); ++it ) {
        opFastAccess.push_back(*it);
    }

    opSet.reserve(model.opSet.size());
    for(std::vector<SetType>::const_iterator it = opSet.cbegin(); it != opSet.cend(); ++it) {
        opSet.push_back(*it);
    }
}

Model::~Model() {
    opSet.clear();
    S1.clear();
    S2.clear();
    opFastAccess.clear();
}

  
void Model::addOperatingPoint(OperatingPointID_t opID) {
  if (opSet[opID] == SetType::NotPresent) {
    S2.push_front(opID);
    opFastAccess[opID] = S2.begin();
    opSet[opID] = SetType::S2;
  }
}


void Model::addOperatingPointToS1(OperatingPointID_t opID) {
  opSet[opID] = SetType::S1;
  S1.push_front(opID);
  opFastAccess[opID] = S1.begin();
}





std::list<OperatingPointID_t>::iterator Model::removeOperatingPointFromS1(OperatingPointID_t opID) {
  
  std::list<OperatingPointID_t>::iterator result;
  
  if (opSet[opID] == SetType::S1) {
    opSet[opID] = SetType::NotPresent;
    result = S1.erase(opFastAccess[opID]);
  }
   
  return result;
}

std::list<OperatingPointID_t>::iterator Model::removeOperatingPointFromS2(OperatingPointID_t opID) {
  
  std::list<OperatingPointID_t>::iterator result;
  
  if (opSet[opID] == SetType::S2) {
    opSet[opID] = SetType::NotPresent;
    result = S2.erase(opFastAccess[opID]);
  }
  
  return result;
}

  
std::list<OperatingPointID_t>::iterator Model::commitOP(OperatingPointID_t opID) {
  S1.push_front(opID);
  std::list<OperatingPointID_t>::iterator previousValue = opFastAccess[opID];
  opFastAccess[opID] = S1.begin();
  opSet[opID] = SetType::S1;
  return S2.erase(previousValue);
}



  
  
} // namespace asrtm

} // namespace argo
