/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ASRTM_OP_MANAGER_H
#define ASRTM_OP_MANAGER_H

#include <list>
#include <vector>
#include <memory>
#include <string>
#include <mutex>



#include "argo/asrtm/constraint.h"
#include "argo/asrtm/observable_characteristic.h"
#include "argo/asrtm/known_characteristic.h"
#include "argo/asrtm/model.h"
#include "argo/asrtm/operating_point.h"
#include "argo/config.h"



namespace argo { namespace asrtm {

  typedef std::list<ConstraintPtr> ConstraintList;


  class OPManager {
  public:

    
    /**
     * @brief Constructor that create the model
     * 
     * @param opList is the list of the operating point
     *
     * @param state is the name of the state handled by this manager
     */
    OPManager(OperatingPointsList opList);
    OPManager(const OPManager& manager);
    ~OPManager();
    
    /**
     * @param changed if it's true is been chosed a dfferent OP
     * @details
     * This method retrieve the best application parameter
     * using the data from the monitors
     */
    ApplicationParameters getBestApplicationParameters(bool* changed = nullptr);


    /**
     * @details
     * These methods handles the creation/destruction of
     * a created constraint
     */

    void addConstraintOnBottom(ConstraintPtr constraint);

    void addConstraintOnTop(ConstraintPtr constraint);

    void addConstraintBefore(ConstraintPosition_t position, ConstraintPtr constraint);

    void addConstraintAfter(ConstraintPosition_t position, ConstraintPtr constraint);
    
    void addRelaxableConstraint(ConstraintPtr constraint);

    void removeConstraintOnBottom();
    
    void removeConstraintOnTop();

    void removeConstraintAt(ConstraintPosition_t position);

    /**
     * @details
     * These methods handles the creation/destruction of
     * a constraint inside the manager, used to hide the
     * construnction of a *Characteristic
     */
    void addStaticConstraintOnBottom(std::string metricName, ComparisonFunction cFun, OPMetric_t value);

    template<typename dataType>
    void addDynamicConstraintOnBottom(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage = 0, argo::monitor::WindowSize validityNumber = 1);

    void addStaticConstraintOnTop(std::string metricName, ComparisonFunction cFun, OPMetric_t value);

    template<typename dataType>
    void addDynamicConstraintOnTop(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage = 0, argo::monitor::WindowSize validityNumber = 1);

    void addStaticConstraintBefore(ConstraintPosition_t position, std::string metricName, ComparisonFunction cFun, OPMetric_t value);

    template<typename dataType>
    void addDynamicConstraintBefore(ConstraintPosition_t position, std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage = 0, argo::monitor::WindowSize validityNumber = 1);

    void addStaticConstraintAfter(ConstraintPosition_t position, std::string metricName, ComparisonFunction cFun, OPMetric_t value);

    template<typename dataType>
    void addDynamicConstraintAfter(ConstraintPosition_t position, std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage = 0, argo::monitor::WindowSize validityNumber = 1);

    void addRelaxableConstraint(std::string metricName, ComparisonFunction cFun, OPMetric_t value, std::function<void()> callbackFunction);

    template<typename dataType>
    void addRelaxableConstraint(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, argo::monitor::WindowSize validityNumber = 1);




    /**
     * @details
     * Change the goal value of a static constraint
     */
    void changeStaticConstraintGoalValue(ConstraintPosition_t position, OPMetric_t newValue);

    void incrementStaticConstraintGoalValue(ConstraintPosition_t position, OPMetric_t incPerc);

    double getStaticConstraintGoalValue(ConstraintPosition_t position);


    /**
     * @details
     * These methods are used to set the operating points' rank
     */
    void setGeometricRank(std::map<std::string, float> structure, RankObjective objective);

    void setLinearRank(std::map<std::string, float> structure, RankObjective objective);

    void setSimpleRank(std::string metricName, RankObjective objective);

    /**
     * @details
     * When an op is changed, all the observation window
     * involved must be cleared from the previous data
     */
    void resetWindows();

    /**
     * @brief get the value of the metric wrt the current operating point
     * @param metricName the name of the selected metric
     * @return it's value
     */
    OPMetric_t getMetricValue(std::string metricName);


    /**
     * @brief check if the model wrt the current state
     * @return true if at least one operating point satisfies all the constraints
     */
    bool isPossible();
    
    
  protected:

    void addConstraint(ConstraintPtr constraint, ConstraintList::iterator position);

    void removeConstraint(ConstraintList::iterator position);
    
    /**
     * @brief Used to get a best op when a constraint empty the set S1
     */
    OperatingPointID_t getBestOPFromUnsatisfiableConstraint();
    
    
    /**
     * @details
     * The best op is retrieved from S1
     */
    OperatingPointID_t getBestOPFromModel();
    
    
    
    /**
     * @brief Checks if the relaxable constraints are achieved
     */
    void checkRelaxableConstraints();
    
    
    /**
     * @brief Check if an op satisfies the constraints
     * 
     * @param strictly If the op must satisfies all the constraints or
     *                 only the ones above the unsatisfiable constraint
     */
    bool isValid(OperatingPointID_t opID, CheckType type);
    
    
    /**
     * @details 
     * Check how many constraint that have lower priority than the
     * unsatisfiable constraint ar satisfied
     * 
     * @param opID1 The id of the first operating point
     * 
     * @param opID2 The id of the second operating point
     * 
     */
    OperatingPointID_t getBest(OperatingPointID_t opID1, OperatingPointID_t opID2);
    
    
    
    /**
     * @details It's the list of ApplicationParameters,
     *          the index of the vector is the ID of
     *          the OP. This list is not sorted, nor
     *          modified
     */
    std::vector<ApplicationParameters> appParams;
    
    
    /**
     * @details 
     * This is used to rappresent the situation and
     * take decision about the best operating point
     */
    ModelPtr model;

    
    /**
     * @brief The constraints that an OP must stasfy
     */
    ConstraintList constraints;
    
    /**
     * @brief The constraints that the manager can relax
     */
    ConstraintList relaxableConstraints;
    
    
    /**
     * @brief How much an operating point is good
     */
    std::vector<Rank_t> ranks;
    
    
    OperatingPointID_t currentBestOP;
    
    /**
     * @details
     * This iterator rappresent the constraint that invalidates all
     * the op in S1.
     */
    ConstraintList::iterator unsatisfiableConstraint;


    /**
     * @details
     * This list contains all the operating points that this
     * manager must handle
     */
    OperatingPointsList opList;

    /**
     * @brief Mutex variable associated to the constraints
     */
    std::mutex constraintMutex;
    
  };

typedef std::shared_ptr<OPManager> OPManagerPtr;

template<typename dataType>
void OPManager::addDynamicConstraintAfter(ConstraintPosition_t position, std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage, argo::monitor::WindowSize validityNumber) {
  // create the ObsrvableCharacteristic
  std::shared_ptr<ObservableCharacteristic<dataType>> oc(new ObservableCharacteristic<dataType>(goal, opList, metricName));
  oc->setElementNumber(validityNumber);
  oc->setNoiseThreshold(noisePercentage);

  // add the constraint
  addConstraintAfter(position, std::dynamic_pointer_cast<Constraint>(oc));
}

template<typename dataType>
void OPManager::addDynamicConstraintBefore(ConstraintPosition_t position, std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage, argo::monitor::WindowSize validityNumber) {
  // create the ObsrvableCharacteristic
  std::shared_ptr<ObservableCharacteristic<dataType>> oc(new ObservableCharacteristic<dataType>(goal, opList, metricName));
  oc->setElementNumber(validityNumber);
  oc->setNoiseThreshold(noisePercentage);
  
  // add the constraint
  addConstraintBefore(position, std::dynamic_pointer_cast<Constraint>(oc));
}


template<typename dataType>
void OPManager::addDynamicConstraintOnTop(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage, argo::monitor::WindowSize validityNumber) {
  // create the ObsrvableCharacteristic
  std::shared_ptr<ObservableCharacteristic<dataType>> oc(new ObservableCharacteristic<dataType>(goal, opList, metricName));
  oc->setElementNumber(validityNumber);
  oc->setNoiseThreshold(noisePercentage);

  // add the constraint
  addConstraintOnTop(std::dynamic_pointer_cast<Constraint>(oc));
}

template<typename dataType>
void OPManager::addDynamicConstraintOnBottom(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, float noisePercentage, argo::monitor::WindowSize validityNumber) {
  // create the ObsrvableCharacteristic
  std::shared_ptr<ObservableCharacteristic<dataType>> oc(new ObservableCharacteristic<dataType>(goal, opList, metricName));
  oc->setElementNumber(validityNumber);
  oc->setNoiseThreshold(noisePercentage);

  // add the constraint
  addConstraintOnBottom(std::dynamic_pointer_cast<Constraint>(oc));
}

template<typename dataType>
void OPManager::addRelaxableConstraint(std::shared_ptr<argo::monitor::Goal<dataType>> goal, std::string metricName, argo::monitor::WindowSize validityNumber) {
  // create the ObservableCharacteristic
  std::shared_ptr<ObservableCharacteristic<dataType>> oc(new ObservableCharacteristic<dataType>(goal, opList, metricName, true));
  oc->setElementNumber(validityNumber);

  // add the constraint
  addRelaxableConstraint(std::dynamic_pointer_cast<Constraint>(oc));
}

} // namespace asrtm

} // namespace argo

#endif // ASRTM_OP_MANAGER_H
