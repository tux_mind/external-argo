/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ASRTM_CONSTRAINT_H
#define ASRTM_CONSTRAINT_H

#include <list>
#include <memory>
#include <stdint.h>
#include <string>


#include "argo/asrtm/model.h"
#include "argo/config.h"



namespace argo { namespace asrtm {
  
  /**
   * @brief Constraint general interface
   * 
   * @details
   * This class provides a general interface for a constraint
   * used to define a quality that an Operating Point must
   * have for being considered accettable.
   * See the class ObservableCharacteristic for further details
   */
  class Constraint {
  public:
    
    /**
     * @brief Check if the constraint is satisfied by the operating point
     * 
     * @param opID Is the id of the operatng point
     * 
     */
    virtual bool isAdmissible(OperatingPointID_t opID) = 0;
    
    /**
     * @brief Handle the new information from monitor
     * 
     * @param model see the class Model for further details
     * 
     * @param currentOP_ID the id of the current operating point
     * 
     * @return If is needed to found another best operating point, i.e. if the
     *         current operating point doesn't satisfy anymore the constraint
     * 
     */
    virtual bool updateModel(ModelPtr model,  OperatingPointID_t currentOP_ID) = 0;
    
    
    /**
     * @brief Check the relaxable goal
     */
    virtual void checkRelaxable(OperatingPointID_t currentOP) = 0;
    
    
    /**
     * @brief This method return the worst op for this metric.
     */
    virtual std::list<OperatingPointID_t>::iterator getWorstOP() = 0;
    
    /**
     * @brief Return an iterator to the best op
     */
    virtual std::list<OperatingPointID_t>::iterator getBestOP() = 0;
    
    /**
     * @brief Return an iterator to the last valid op
     */
    virtual std::list<OperatingPointID_t>::iterator getLimitOP() = 0;
    
    /**
     * @brief Compute the distance between the goal and the op
     */
    virtual float getDistance(OperatingPointID_t op) = 0;
    
    /**
     * @brief Delete all the observation window's elements
     */
    virtual void clearWindow() = 0;

    virtual std::string getName() = 0;

    
    virtual ~Constraint() {}
      
  };
  
  typedef std::shared_ptr<Constraint> ConstraintPtr;

} // namespace asrtm


} // namespace argo

#endif // ASRTM_CONSTRAINT_H
